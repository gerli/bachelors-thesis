def sphere_size(radius, n=5, q=4):
    # The size of a sphere (in number of elements)
    elements = 0
    for i in range(radius+1):
        elements += binomial(n, i) * (q-1)**i
    return elements

def sphere_size_pairs(radius, n=5, q=4):
    # The size of a sphere (in number of pairs)
    elements = sphere_size(radius, n, q)
    return binomial(elements, 2)

def all_near_duplicates(n, q, d):
    pairs = 0
    for distance in range(1, d+1):
        pairs += binomial(n, distance) * (q-1)^distance * q^n / 2
    return pairs

def single_near_duplicates(n, q, d):
    if d == 1:
        return n * binomial(q, 2)
    elif d == 2:
        return single_near_duplicates(n, q, 1) + binomial(n,2)*(q-1)^2 * (2*n*(q-1) + q^2 - 2*q + 4)/2
    else:
        raise ValueError("d>2 not implemented")


def distances_between_codewords(code):
    dists = [[] for __ in range(len(code)-1)]
    for c1 in range(len(code) - 1):
        for c2 in range(c1+1, len(code)):
            dists[c1].append((code[c1]+code[c2]).hamming_weight())
    return dists
print "\n".join(map(lambda x: " ".join(x), map(lambda x: map(str, x), distances_between_codewords(c))))

def baseline_pairs(n, q, d):
    # this is the total number of sequences, not for N
    if n % (d+1) == 0:
        k = n / (d+1)
        nr_buckets = q^k
        elements_in_bucket = q^(n-k)
        pairs_in_bucket = binomial(elements_in_bucket, 2)
        return (d+1) * nr_buckets * pairs_in_bucket
    else:
        raise ValueError("n is not divisible by d+1, this case is not yet handled")
        

        