filename="thesis"
latex --shell-escape $filename.tex
bibtex $filename.aux
latex --shell-escape $filename.tex
latex --shell-escape $filename.tex
dvipdf $filename.dvi

rm $filename.aux $filename.bbl $filename.blg $filename.dvi $filename.log $filename.toc $filename.out

evince $filename.pdf
