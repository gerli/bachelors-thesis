#### An experiment to determine how many pairs need saving
## The field we're working in - GF(4)
field = GF(4, 'a')

## We have vectors of length 5, we can detect all with distance 1
# 5 = (4^2 - 1)/(4-1)
c = codes.HammingCode(2, field)

## Corresponding vector space:
V = VectorSpace(field, 5)

## The number of all pairs in the vector space:
binomial(len(V), 2) # 523776

## The number of (unordered) pairs that have distance 1:
close_pairs = []
for x1 in range(len(V)-1):
    vector1 = V[x1]
    for x2 in range(x1+1, len(V)):
        vector2 = V[x2]
        distance = (vector1 + vector2).hamming_weight()
        if distance <= 1:
            close_pairs.append((vector1, vector2))
len(close_pairs) # 7680

## How many of these pairs are in the same ball (decode to the same codeword):
not_in_same_ball = []
for vector1, vector2 in close_pairs:
    if c.decode(vector1) != c.decode(vector2):
        not_in_same_ball.append((vector1, vector2))
len(not_in_same_ball) # 5760
len(close_pairs) - len(not_in_same_ball) # 1920 - these were rescued

## How many pairs do the balls around codewords actually contain:
def ball_size(radius, n=5, q=4):
    # this calculates the size of a ball (in number of pairs)
    elements = 0
    for i in range(radius+1):
        elements += binomial(n, i) * (q-1)**i
    return binomial(elements, 2)
    
len(c)*ball_size(1) # 7680
len(c)*ball_size(1) - (len(close_pairs)-len(not_in_same_ball)) # these 5760 are falsely taken along

## Let us shift all the vectors by (1,0,0,0,0) and see how many pairs we can now rescue
# note: this is theoretically equivalent to shifting the codewords by the same vector but shifting the vectors is easier to program
shift = V((1,0,0,0,0))
not_in_same_ball1 = []
for vector1, vector2 in not_in_same_ball:
    vector1 += shift
    vector2 += shift
    if c.decode(vector1) != c.decode(vector2):
        not_in_same_ball1.append((vector1+shift, vector2+shift))

len(not_in_same_ball) - len(not_in_same_ball1) # 1536 - this many were saved
float(5760 / 1536) # 3.5 - theoretically we will need 4 of these shifts

def differences(numbers):
    diffs = []
    for i in range(len(numbers)-1):
        diffs.append(numbers[i] - numbers[i+1])
    return diffs

shifts = [V((1,0,0,0,0)), V(('a',0,0,0,0)), V(('a+1',0,0,0,0)), V((0,0,0,0,1)), V((0,1,0,0,0)), V((0,0,0,1,0)), V((0,0,1,0,0))]
not_in_same_balls = [[] for _ in shifts]
not_in_same_balls.insert(0, not_in_same_ball)
for i in range(len(shifts)):
    for vector1, vector2 in not_in_same_balls[i]:
        vector1 += shifts[i]
        vector2 += shifts[i]
        if c.decode(vector1) != c.decode(vector2):
            not_in_same_balls[i+1].append((vector1+shifts[i], vector2+shifts[i]))
differences(map(len, not_in_same_balls))
# we see that 6 shifts do the job
# can we do better?

# the first 3 shifts are great, they save 1536 pairs each
# let's try to find a shift that saves the most from here on
def translation(positions, elements, space=V):
    # create a translation vector in this space with elements at specified positions
    if len(positions) != len(elements):
        raise ValueError("Number of letters should correspond to the number of positions!")
    vector = [0 for _ in range(space.dimension())]
    for i in range(len(positions)):
        vector[positions[i]] = elements[i]
    return space(vector)

# let's try all single-weight vectors:
for pos in range(5):
    for element in set(field.list()) - {field.zero()}:
        shift_vec = translation([pos], [element])
        count = 0
        for vector1, vector2 in not_in_same_balls[3]:
            vector1 += shift_vec
            vector2 += shift_vec
            if c.decode(vector1) == c.decode(vector2):
                count += 1
        print pos, element, count
# apparently all the ones we've done already save none more (obvious)
# all others can save 384

# What about two-weight shifts?
for pos1 in range(V.dimension()-1):
    for pos2 in range(pos1+1, V.dimension()):
        for element1 in nonzero_elements:
            for element2 in nonzero_elements:
                shift_vec = translation([pos1,pos2], [element1,element2])
                count = 0
                for vector1, vector2 in not_in_same_balls[3]:
                    vector1 += shift_vec
                    vector2 += shift_vec
                    if c.decode(vector1) == c.decode(vector2):
                        count += 1
                print shift_vec, count
# some of these can also save 384 pairs, some 0, for example
#(0, 1, 1, 0, 0) 0
#(0, 0, a, 0, a + 1) 0
#(0, 0, a + 1, 0, 1) 0
#(0, 0, 0, a + 1, a) 0


################# SOME QUESTIONS ANSWERED

### Which is the best shift out of all single-weight shifts:
nonzero_elements = set(field.list()) - {field.zero()}
for pos in range(5):
    for element in nonzero_elements:
        shift_vec = translation([pos], [element])
        count = 0
        for vector1, vector2 in not_in_same_ball:
            vector1 += shift_vec
            vector2 += shift_vec
            if c.decode(vector1) == c.decode(vector2):
                count += 1
        print pos, element, count
### Result: any one of these shifts will result in rescuing 1536 pairs
### Exact output:
#0 1 1536
#0 a 1536
#0 a + 1 1536
#1 1 1536
#1 a 1536
#1 a + 1 1536
#2 1 1536
#2 a 1536
#2 a + 1 1536
#3 1 1536
#3 a 1536
#3 a + 1 1536
#4 1 1536
#4 a 1536
#4 a + 1 1536

    
### What about two-weight shifts?
for pos1 in range(V.dimension()-1):
    for pos2 in range(pos1+1, V.dimension()):
        for element1 in nonzero_elements:
            for element2 in nonzero_elements:
                shift_vec = translation([pos1,pos2], [element1,element2])
                count = 0
                for vector1, vector2 in not_in_same_ball:
                    vector1 += shift_vec
                    vector2 += shift_vec
                    if c.decode(vector1) == c.decode(vector2):
                        count += 1
                        #break # quicker test - will save at least 1?
                print shift_vec, count
### Result: any one of these shifts will also result in rescuing 1536 pairs. 


### Can I construct a better sequence of functions than rescuing 1536, 1536, 1536, 384, 384, 384 pairs?
def all_1_shifts(space):
    f = space.base_field()
    nonzero_elements = set(f) - {f.zero()}
    for pos in range(space.dimension()):
        for element in nonzero_elements:
            yield translation([pos], [element])

def all_2_shifts(space):
    f = space.base_field()
    nonzero_elements = set(f) - {f.zero()}
    for pos1 in range(space.dimension()-1):
        for pos2 in range(pos1+1, space.dimension()):
            for element1 in nonzero_elements:
                for element2 in nonzero_elements:
                    yield translation([pos1,pos2], [element1,element2])

def get_unsaved_pairs(pairs, shift):
    unsaved_pairs = []
    for vector1, vector2 in pairs:
        vector1 += shift
        vector2 += shift
        if c.decode(vector1) != c.decode(vector2):
            unsaved_pairs.append((vector1+shift, vector2+shift))
    return unsaved_pairs

one_shifts = [x for x in all_1_shifts(V)]
two_shifts = [x for x in all_2_shifts(V)]

# start with a one-shift and then try the next ones:
shift1 = one_shifts[0]
unsaved1 = get_unsaved_pairs(not_in_same_ball, shift1)
shifts_rest = one_shifts[1:] + two_shifts
ntest = 0
for shift2 in shifts_rest:
    unsaved2 = get_unsaved_pairs(unsaved1, shift2)
    if len(unsaved1) - len(unsaved2) < 1536:
        continue
    for shift3 in shifts_rest:
        unsaved3 = get_unsaved_pairs(unsaved2, shift3)
        if len(unsaved2) - len(unsaved3) < 1536:
            continue
        for shift4 in shifts_rest:
            ntest += 1
            print ntest
            unsaved4 = get_unsaved_pairs(unsaved3, shift4)
            if len(unsaved3) - len(unsaved4) <= 384:
                continue
            print shift1, shift2, shift3, shift4, len(unsaved3)-len(unsaved4)

#sage: shifts_rest.index(shift2)
#77

### same experiment with a slightly better setup:
good_combinations = []
shift1 = one_shifts[0]
unsaved1 = get_unsaved_pairs(not_in_same_ball, shift1)
shifts_rest = one_shifts[1:] + two_shifts
ntest = 0
for shift2 in shifts_rest:
    unsaved2 = get_unsaved_pairs(unsaved1, shift2)
    if len(unsaved1) - len(unsaved2) < 1536:
        ntest += len(shifts_rest)*len(shifts_rest)
        continue
    for shift3 in shifts_rest:
        unsaved3 = get_unsaved_pairs(unsaved2, shift3)
        if len(unsaved2) - len(unsaved3) < 1536:
            ntest += len(shifts_rest)
            continue
        for shift4 in shifts_rest:
            ntest += 1
            print ntest
            unsaved4 = get_unsaved_pairs(unsaved3, shift4)
            if len(unsaved3) - len(unsaved4) <= 384:
                continue
            good_combinations.append((shift1, shift2, shift3, shift4))
            print shift1, shift2, shift3, shift4, len(unsaved3)-len(unsaved4)

shift1 = two_shifts[0]
unsaved1 = get_unsaved_pairs(not_in_same_ball, shift1)
shifts_rest = one_shifts + two_shifts[1:]
ntest = 0
for shift2 in shifts_rest:
    unsaved2 = get_unsaved_pairs(unsaved1, shift2)
    if len(unsaved1) - len(unsaved2) < 1000:
        ntest += len(shifts_rest)*len(shifts_rest)
        continue
    for shift3 in shifts_rest:
        unsaved3 = get_unsaved_pairs(unsaved2, shift3)
        if len(unsaved2) - len(unsaved3) < 1000:
            ntest += len(shifts_rest)
            continue
        for shift4 in shifts_rest:
            ntest += 1
            print ntest
            unsaved4 = get_unsaved_pairs(unsaved3, shift4)
            if len(unsaved3) - len(unsaved4) <= 384:
                continue
            if len(unsaved4) == 0:
                good_combinations.append((shift1, shift2, shift3, shift4, shift5))
                print [shift1, shift2, shift3, shift4, shift5]
            else:
                for shift5 in shifts_rest:
                    ntest += 1
                    print 5, ntest
                    unsaved5 = get_unsaved_pairs(unsaved4, shift5)
                    if len(unsaved5) > 0:
                        continue
                    good_combinations.append((shift1, shift2, shift3, shift4, shift5))
                    print [shift1, shift2, shift3, shift4, shift5]
            
def consecutive_shifts(shifts):
    not_in_same_balls = [[] for _ in shifts]
    not_in_same_balls.insert(0, not_in_same_ball)
    for i in range(len(shifts)):
        for vector1, vector2 in not_in_same_balls[i]:
            vector1 += shifts[i]
            vector2 += shifts[i]
            if c.decode(vector1) != c.decode(vector2):
                not_in_same_balls[i+1].append((vector1+shifts[i], vector2+shifts[i]))
    return(differences(map(len, not_in_same_balls)))
consecutive_shifts(map(V, [(1, 1, 0, 0, 0), (1, 0, 0, 0, 0), ('a', 0, 0, 0, 0), ('a + 1', 0, 0, 0, 0)]))





#### TEST WITH OTHER CODES
c = codes.BCHCode(7, 3, GF(4, 'a'))

nr_pairs = []
for cw in c1:
    single_neighbors = []
    for vec in c1.ambient_space():
        if (vec + cw).hamming_weight() in [2]:
            single_neighbors.append(vec)

    single_close_pairs = []
    for x1 in range(len(single_neighbors)-1):
        vector1 = single_neighbors[x1]
        for x2 in range(x1+1, len(single_neighbors)):
            vector2 = single_neighbors[x2]
            distance = (vector1 + vector2).hamming_weight()
            if distance in [1,2]:
                single_close_pairs.append((vector1, vector2))
    #single_close_pairs
    nr_pairs.append(len(single_close_pairs))
sum(nr_pairs)

all_one = []
for vec1, vec2 in single_close_pairs:
    if (cw + vec1).hamming_weight() == 1 and (cw + vec2).hamming_weight() == 1 and (vec1 + vec2).hamming_weight() == 2:
        all_one.append((vec1, vec2))
len(all_one)

all_one = []
for vec1, vec2 in single_close_pairs:
    if (cw + vec1).hamming_weight() == 0 and (cw + vec2).hamming_weight() == 2 and (vec1 + vec2).hamming_weight() == 2:
        all_one.append((vec1, vec2))
len(all_one)

all_one = []
for vec1, vec2 in single_close_pairs:
    if (cw + vec1).hamming_weight() == 1 and (cw + vec2).hamming_weight() == 2 and (vec1 + vec2).hamming_weight() == 2:
        all_one.append((vec1, vec2))
len(all_one)