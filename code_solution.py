from collections import defaultdict
from itertools import combinations

def find_pairs(sequences, code, shifts):
    #distance = floor((code.minimum_distance()-1) / 2)
    for shift in shifts:
        spheres = defaultdict(list)
        for sequence in sequences:
            hash = code.decode(sequence-shift)
            spheres[tuple(hash)].append(sequence)
        for hash in spheres:
            for pair in combinations(spheres[hash], 2):
                yield pair
                
field = GF(4, 'a')
code = codes.HammingCode(2, field)
V = code.ambient_space()
shifts = [V((0,0,0,0,0)), V((1,0,0,0,0)), V(('a',0,0,0,0)), V(('a+1',0,0,0,0)), V((0,1,0,0,0)), V((0,0,0,1,0)), V((0,0,0,0,1))]

N = 100
sequences = random_matrix(field, N, code.length())
distance = floor((code.minimum_distance()-1)/2)
for pair in find_pairs(sequences, code, shifts):
    print pair
    
print 

