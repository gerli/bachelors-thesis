from random import randint, shuffle
from copy import deepcopy

ball = "Mary kicked a \\textcolor{{{0}}}{{{0}}} ball."

colors = ["red", "blue", "green", "black", "cyan", "magenta", "purple"]

for __ in range(10):
    print ball.format(colors[randint(0,len(colors)-1)])
    
    
alphabet = "ATGC"
length = 5

for __ in range(100):
    print "".join([alphabet[randint(0,len(alphabet)-1)] for __ in range(36)]) #, "\\\\"

sequences = []
for __ in range(10):
    sequences.append([alphabet[randint(0,len(alphabet)-1)] for __ in range(length)])
    
for i in range(10):
	for copies in range(3):
		new = deepcopy(sequences[i])
		for mut in range(1):
			mut = randint(0,length-1)
			while sequences[i][mut] == new[mut]:
				new[mut] = alphabet[randint(0,len(alphabet)-1)]
		sequences.append(new)
		
sortseq = sorted(map(lambda x: "".join(x), sequences))
getindex = [0,1,2,3,4,17,18,19,25,26,27,28]
getindex = [1,3,4,18,19,28]
#shuffle(getindex)
for i in getindex:
	print sortseq[i]
	
#shuff = deepcopy(getindex)
#shuffle(shuff)
vals = "abcdef"

for i in range(5):
	for j in range(i+1,6):
		print "\\seq" + vals[i] + " \\  \\seq" + vals[j] + " \\\\"

coloring = {"A": "\\textcolor{blue}{A}", "T": "\\textcolor{red}{T}", "C": "\\textcolor{LimeGreen}{C}", "G": "\\textcolor{purple}{G}"}
for j,i in enumerate(shuff):
	seq = list(sortseq[i])
	print ("\\newcommand{\seq%s}{" + ("".join(map(lambda x: coloring[x], seq))) + "}") % vals[j]
		
for seq in sorted([sortseq[i] for i in getindex]):
	print "".join(map(lambda x: coloring[x], seq)), "\\\\"