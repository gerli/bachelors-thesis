#### An experiment on the naive solution - use functions that take some set of the coordinates to find pairs

from collections import Counter

## The field we're working in - GF(4)
field = GF(4, 'a')

## We have vectors of length 5, we can detect all with distance 1
# 5 = (4^2 - 1)/(4-1)
c = codes.HammingCode(2, field)

## Corresponding vector space:
V = VectorSpace(field, 5)

## The number of (unordered) pairs that have distance 1:
close_pairs = []
for x1 in range(len(V)-1):
    vector1 = V[x1]
    for x2 in range(x1+1, len(V)):
        vector2 = V[x2]
        distance = (vector1 + vector2).hamming_weight()
        if distance <= 1:
            close_pairs.append((vector1, vector2))
len(close_pairs) # 7680

### first function: take the first three elements of a vector
### if they collide, put them in the same ball
balls = {}
for x1 in range(len(V)-1):
    vector1 = V[x1]
    key = vector1[:3]
    for x2 in range(x1+1, len(V)):
        vector2 = V[x2]
        if key == vector2[:3]:
            if tuple(key) not in balls:
                balls[tuple(key)] = []
            balls[tuple(key)].append((vector1, vector2))
len(balls) # 64
map(len, balls.values()) # each ball has 120 pairs - a total of 7680 values
binomial(4**2, 2) * 4**3

### second function: take the last two elements of a vector
### if they collide, put them in the same ball
balls2 = {}
for x1 in range(len(V)-1):
    vector1 = V[x1]
    key = vector1[3:]
    for x2 in range(x1+1, len(V)):
        vector2 = V[x2]
        if key == vector2[3:]:
            if tuple(key) not in balls2:
                balls2[tuple(key)] = []
            balls2[tuple(key)].append((vector1, vector2))
len(balls2) # 16
map(len, balls2.values()) # each ball has 2016 pairs - a total of 32256 values
binomial(4**3, 2) * 4**2

### 7680+32256=39936 is the total number of pairs that we need to look through

# We can test if 3/2 split was a good idea:
a=3; b=5-a; binomial(4**a, 2) * 4**b + binomial(4**b, 2) * 4**a # 39936
# Is a 4/1 split better?
a=4; b=5-a; binomial(4**a, 2) * 4**b + binomial(4**b, 2) * 4**a # 132096 - no, it is worse




#### ACTUAL IMPLEMENTATION
space = V
from collections import defaultdict
from itertools import combinations

def f(coordinates):
    return lambda x: tuple(x.list_from_positions(coordinates))

def is_near_duplicate(pair, distance):
    x, y = pair
    return (y-x).hamming_weight() <= distance

def find_pairs(sequences, distance, space):
    ## The following is ugly but works
    ## First, create the hash functions
    n = space.degree()
    # k - the length of the (longer) subsequences
    k = ceil(n/(distance+1))
    n_short = k * (distance+1) - n
    # n_long = n % (distance+1) #??? Does it work
    hash_functions = []
    start = 0
    for i in range(distance+1):
        if i < n_short:
            length = k-1
        else:
            length = k
        hash_functions.append(f(range(start, start+length)))
        start += length
    
    ## Second, create buckets for each function
    ## Form unordered pairs for each bucket
    for fun in hash_functions:
        buckets = defaultdict(list)
        for sequence in sequences:
            hash = fun(sequence)
            buckets[hash].append(sequence)
        for hash in buckets:
            for pair in combinations(buckets[hash], 2):
                yield pair, is_near_duplicate(pair, distance)
field = GF(4, 'a')
space = VectorSpace(field, 5)              
sequences = []
while len(sequences) < 100:
    elem = space.random_element()
    if elem not in sequences:
        sequences.append(elem)
distance = 1
for pair in find_pairs(sequences, distance, space):
    print pair
    
print 


### EMPIRICAL TEST
#sum([q^k * binomial(q^(n-k), 2) for k in [2,1,1,1]])
def count_pairs(sequences, hash_functions, distance):
    total = 0
    near_duplicates = 0
    bucket_lengths = {}
    for fun in hash_functions:
        buckets = defaultdict(list)
        for sequence in sequences:
            hash = fun(sequence)
            buckets[hash].append(sequence)
        bucket_lengths[fun] = Counter(map(len, buckets.values()))
        for hash in buckets:
            for pair in combinations(buckets[hash], 2):
                total += 1
                if is_near_duplicate(pair, distance):
                    near_duplicates += 1
    return total, near_duplicates, bucket_lengths

def generate_data(field, length, N):
    data = zero_matrix(field, N, length)
    for i in range(N):
        for j in range(length):
            data[i,j] = field.random_element()
    return data
                
q = 4
n = 5
N = 100
d = 1
import sys
from time import time

with open("baseline_321_10tests_vol3.csv", "a") as out_file:
    out_file.write("q,n,N,d,candidates,actuals,bucketlen,bucketfill\n")
    nr_of_pairs = defaultdict(list)
    for d in [3,2,1]:
        set_random_seed(0)
        print "At d", d
        for q in [23, 2]:
            print "\tAt q", q
            field = GF(q, 'a')
            for n in [5,12,36]:
                print "\t\tAt n", n
                space = VectorSpace(field, n)
                
                # k - the length of the (longer) subsequences
                k = ceil(n/(d+1))
                n_short = k * (d+1) - n
                # n_long = n % (distance+1) #??? Does it work
                hash_functions = []
                start = 0
                for i in range(d+1):
                    if i < n_short:
                        length = k-1
                    else:
                        length = k
                    hash_functions.append(f(range(start, start+length)))
                    start += length
                for N in [10^(i+1) for i in range(min([floor(log(q^n, 10)), 6]))]:
                    print "\t\t\tAt N", N
                    sys.stdout.flush()
                    for tries in range(10):
                        # Sample N sequences from the vector space
                        sequences = random_matrix(field, N, n)
                        try:
                            key = (q,n,N,d)
                            element = count_pairs(sequences, hash_functions, d)
                            nr_of_pairs[key].append(element)
                            out_file.write(",".join(map(str, key)))
                            out_file.write(",")
                            out_file.write(",".join(map(str, element[:2])))
                            for fun in element[2]:
                                out_file.write(",")
                                out_file.write(str(len(fun(space.zero()))))
                                out_file.write(",")
                                out_file.write("\"" + str(sorted(element[2][fun].items())) + "\"")
                            out_file.write("\n")
                            out_file.flush()
                        except KeyboardInterrupt:
                            break

    
