from collections import Counter
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

### Partly from sage-6.1.1-x86_64-Linux/src/sage/coding/decoder.py:
def list_decode(C, v):
    V = C.ambient_space()
    if not isinstance(v, list):
        v = list(v)
    v = V(v)
    coset = [[c, (c + v).hamming_weight()] for c in C]
    coset.sort(lambda x, y: x[1] - y[1])
    mindist = coset[0][1]
    if mindist == 0: 
        # v is a codeword
        return [coset[0][0]]
    else:
        # v is at distance 1 from codeword
        close = []
        for cw, weight in coset:
            if weight > 2:
                break
            close.append(cw)
        return close

field = GF(4, 'a')

## Length 5, detect all with distance 1
c = codes.HammingCode(2, field)

### Test how many pairs of distance 1 are in different buckets:
V = VectorSpace(field, 5)
othersames = []
for i in xrange(len(V)):
    first = c.decode(V[i])
    same = 0
    other = Counter()
    for j in xrange(len(V)):
        distance = (V[i] + V[j]).hamming_weight()
        if distance == 1:
            second = c.decode(V[j])
            if first == second:
                same += 1
            else:
                other[tuple(second)] += 1
    othersames.append((same, tuple([x[1] for x in other.most_common()])))
    
Counter(othersames)

### Size of balls:
ballsize = Counter()
for i in xrange(len(V)):
    word = c.decode(V[i])
    ballsize[tuple(word)] += 1

# get a word (not codeword) and all its 1-neighbors:
word = c[1] + V((0,0,0,0,'a'))
# is it a codeword?
c.decode(word) == word

neighbors = []
for vector in V:
    distance = (word + vector).hamming_weight()
    if distance == 1:
        neighbors.append(vector)

# find corresponding codewords
codewords = [c.decode(vector) for vector in neighbors]

# if the codeword is the same as for the word itself, it is already in the same ball
faraway_neighbors = filter(lambda vec: c.decode(vec) != c.decode(word), neighbors)

[(c.decode(vector) + c.decode(word)).hamming_weight() for vector in neighbors]

### What kind of translation should I make in order to get as many balls as possible into a new bin?
def translation(positions, elements, space=V):
    # create a translation vector in this space with elements at specified positions
    if len(positions) != len(elements):
        raise ValueError("Number of letters should correspond to the number of positions!")
    vector = [0 for _ in range(space.dimension())]
    for i in range(len(positions)):
        vector[positions[i]] = elements[i]
    return space(vector)

for pos in range(V.dimension()):
    for element in set(field.list()) - {field.zero()}:
        trans_vec = translation([pos], [element])
        translated_neighbors = [trans_vec + vector for vector in neighbors]
        translated_self = word + trans_vec
        print pos, element, len(filter(lambda vec: c.decode(vec) == c.decode(translated_self), translated_neighbors))

# The best translation is adding the word itself - it will centre the ball at the word

# Testing if adding the difference to neighbors will make all neighbors fall to the same ball:
def translation_test():
    # take random word
    word = V.random_element()
    # decode and find difference
    difference = word - c.decode(word)
    # find neighbors
    neighbors = []
    for vector in V:
        distance = (word + vector).hamming_weight()
        if distance <= 1:
            neighbors.append(vector)

    # add difference to all and get codewords
    codewords = [c.decode(difference + vector) for vector in neighbors]
    print difference, len(set(map(tuple, codewords)))

# Testing if adding the difference to all words makes only the words that are at distance 1 fall into the same ball
def translation_test2():
    # take random word
    word = V.random_element()
    # decode and find difference
    decoded = c.decode(word)
    difference = word - decoded
    
    # find neighbors
    neighbors = []
    distances = []
    for vector in V:
        vec_decode = c.decode(vector + difference)
        if vec_decode == decoded:
            distance = (word + vector).hamming_weight()
            neighbors.append(vector)
            distances.append(distance)
    #print neighbors
    print word, decoded, len(neighbors), Counter(distances)


neighbors = []
for vector in V:
    distance = (word + vector).hamming_weight()
    if distance == 1:
        neighbors.append(vector)

# if the codeword is the same as for the word itself, it is already in the same ball
faraway_neighbors = filter(lambda vec: c.decode(vec) != c.decode(word), neighbors)

### We need to find a group of translations that is independent of the word
nonzero_elements = set(field.list()) - {field.zero()}
def get_2translations(word):
    vectors = []
    for pos1 in range(V.dimension()-1):
        for pos2 in range(pos1+1, V.dimension()):
            for element1 in nonzero_elements:
                for element2 in nonzero_elements:
                    trans_vec = translation([pos1,pos2], [element1,element2])
                    translated_neighbors = [trans_vec + vector for vector in faraway_neighbors]
                    translated_self = word + trans_vec
                    # pos1, pos2, element1, element2
                    impact = len(filter(lambda vec: c.decode(vec) == c.decode(translated_self), translated_neighbors))
                    if impact > 0:
                        print impact, trans_vec
                        #vectors.append(tuple(trans_vec))
    return vectors
 
set(get_2translations(V((0,0,0,0,'a')))) & set(get_2translations(V((0,0,0,'a',0)))) & set(get_2translations(V((0,0,'a',0,0)))) & set(get_2translations(V((0,'a',0,0,0)))) & set(get_2translations(V(('a',0,0,0,0)))) 


### ... but of 1-translations


### LOWER BOUND TEST
# pick a codeword
cw = c[0]

# set the parameters
d = 1
r = 1

# get all the words for the first element in pair - which are at distance more than r-d but up to r from cw:
W1 = []
for vector in V:
    distance = (cw + vector).hamming_weight()
    if distance > r-d and distance <= r:
        W1.append(vector)
        
# get all the words for the second element in pair for one vector - which are at distance up to d from the word but don't decode to cw
lens = []
pairs = []
for word in W1:
    W2 = []
    for vector in V:
        distance = (word + vector).hamming_weight()
        if distance <= d and c.decode(vector) != cw:
            W2.append(vector)
            pairs.append((word, vector))
    lens.append(len(W2))

# the number of troublesome pairs:
sum(lens)

def calc_troublesome(r, d, n, q=4):
    first = 0
    for i in range(d, r+1):
        first += binomial(n, i) * (q-1)**i
    second = 0
    for i in range(1, d+1):
        second += binomial(n-r, i) * (q-1)**i
    return first * second
    
def calc_rescuable(d, n, q=4):
    # this calculates the size of a ball (in number of pairs) with radius d
    elements = 0
    for i in range(d+1):
        elements += binomial(n, i) * (q-1)**i
    return binomial(elements, 2)

float(calc_troublesome(1, 1, 5) / calc_rescuable(1, 5))

## calculate the number of pairs that required saving in a ball with radius d:
word = W1[1]              # at distance 1 from cw
word = V(('a','a',0,0,0)) # at distance 2 from cw
rescued = []
for v1, v2 in pairs:
    distance1 = (word + v1).hamming_weight()
    distance2 = (word + v2).hamming_weight()
    if distance1 <= d and distance2 <= d: # pair is in ball
        rescued.append((v1, v2))

# the number of rescued pairs by ball with 120 pairs
len(rescued) # distance 1 - 12 out of 180 - they all have the same first element -> word
len(rescued) # distance 2 - 6 out of 180


cws = []
for word in W1:
    for vector in V:
        distance = (word + vector).hamming_weight()
        if distance <= d and c.decode(vector) != cw:
            cws.append(tuple(c.decode(vector)))


            pairs.append((word, vector))            